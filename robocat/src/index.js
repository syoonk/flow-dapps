import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootswatch/dist/minty/bootstrap.min.css';
import * as fcl from "@onflow/fcl";
import * as types from "@onflow/types";

fcl.config()
  .put("accessNode.api", "https://rest-testnet.onflow.org")
  .put("discovery.wallet", "https://fcl-discovery.onflow.org/testnet/authn")
  .put("0xRoboCat", "0x69f77b205515efbb")
  .put("0xNonFungibleToken", "0x631e88ae7f1d7c20");

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App fcl={ fcl } types={ types } />
  </React.StrictMode>
);