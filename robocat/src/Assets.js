import { useState, useEffect } from "react";

import { getAllDataScript } from "./cadence/code";

const Assets = ({ fcl, types }) => {

  const [currentUser, setCurrentUser] = useState("");
  const [assets, setAssets] = useState([]);

  const getAssets = async () => {
    try {
      const response = await fcl.send([
        fcl.script`${getAllDataScript}`,
        fcl.args([ fcl.arg(currentUser, types.Address) ]),
        fcl.limit(9999)
      ])
      .then(fcl.decode);

      setAssets(response);

      console.log(response);
    } catch(e) {
      console.log("ERROR::getAssets::\n");
      console.log(e);

      setAssets([]);
    }
  }

  useEffect(() => {
    const init = async () => {
      const _currentUser = (await fcl.currentUser.snapshot()).addr;
      setCurrentUser(_currentUser);
    }
    init();
  }, []);

  useEffect(() => {
    (async () => await getAssets())()
  }, [currentUser])

  return (
    <div>
      <div style={{ display: "flex" }}>
        <h1>Assets</h1>
        <button className="btn btn-link" onClick={() => getAssets()}>refresh</button>
      </div>
      <div>
        <table className="table table-hover">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Image</th>
            </tr>
          </thead>
          <tbody>
          {
            assets.map((_asset, idx) =>
              <tr className="table-light" key={"robocat" + String(idx)}>
                <th scope="row">{ _asset.name }</th>
                <td><img src={_asset.image} style={{ height: "50px", width: "50px" }}></img></td>
              </tr>
            )
          }
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default Assets;