import { useState, useEffect } from "react";

const Wallet = ({ fcl }) => {

  const [user, setUser] = useState({ addr: "" });

  const login = async () => {
    await fcl.authenticate();
  }

  const logout = async () => {
    await fcl.unauthenticate();
  }

  useEffect(() => {
    const initPage = async () => {
      fcl.currentUser.subscribe(setUser);
    }
    initPage();
  }, []);


  return (
    <div>
      <div>
      { user.addr && user.addr !== "" ? (
          <h2>Current user is: {user.addr}</h2>
        ) : (
          <h2>Wallet is not connected</h2>
        )
      }
      </div>
      <div>
      { !user.addr || user.addr === "" ? (
          <button
            onClick={login}
            type="button"
            className="btn btn-primary"
          >Login</button>

        ) : (
          <button
            onClick={logout}
            type="button"
            className="btn btn-secondary"
          >Logout</button>
        )
      }
      </div>
    </div>
  )
}

export default Wallet;