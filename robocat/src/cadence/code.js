export const minterTx = `
  import RoboCat from 0xRoboCat
  import NonFungibleToken from 0xNonFungibleToken

  transaction(recipient: Address) {

  prepare(acct: AuthAccount) {
    let nftMinter = acct.borrow<&RoboCat.NFTMinter>(from: /storage/RoboCatMinter)
      ?? panic("Cannot borrow NFTMinter resource")

    let collectionRef = getAccount(recipient).getCapability(/public/RoboCatCollection)
        .borrow<&RoboCat.Collection{RoboCat.CollectionPublic}>()
        ?? panic("The account doesn't have RoboCat collection")

    collectionRef.deposit(token: <- nftMinter.createNFT())
  }

    execute {
      log("NFT has been deposited to ".concat(recipient.toString()))
    }
  }
`

export const getAllDataScript = `
  import RoboCat from 0xRoboCat

  pub fun main(account: Address): [AnyStruct] {
    let nftCollection = getAccount(account).getCapability(/public/RoboCatCollection)
      .borrow<&RoboCat.Collection{RoboCat.CollectionPublic}>()
      ?? panic("Cannot borrow RoboCat Collection")

    let ids: [UInt64] = nftCollection.getIDs()

    var data: [AnyStruct] = []
    for _id in ids {
      let tokenRef: &RoboCat.NFT = nftCollection.borrowEntireNFT(id: _id)

      data.append({ "name": tokenRef.name, "image": tokenRef.image })
    }

    return data
  }
`

export const createCollection = `
  import RoboCat from 0xRoboCat
  import NonFungibleToken from 0xNonFungibleToken

  transaction {

    prepare(acct: AuthAccount) {
      log("Signer is: ".concat(acct.address.toString()))
      log("Creating Collection...")

      acct.save(<- RoboCat.createEmptyCollection(), to: /storage/RoboCatCollection)
      acct.link<&RoboCat.Collection{RoboCat.CollectionPublic}>(/public/RoboCatCollection, target: /storage/RoboCatCollection)
    }

    execute {
      log("Stored a collection for RoboCat")
    }

  }
`