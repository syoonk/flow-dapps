import { useState } from "react";

import { minterTx, createCollection } from "./cadence/code";

const Mint = ({ fcl, types }) => {

  const [processing, setProcessing] = useState(false);
  const [recipient, setRecipient] = useState("");

  const sleep = ms => {
    return new Promise((r) => setTimeout(r, ms));
  }

  const waitingForTx = async _txHash => {
    while(true) {
      const status = await fcl
        .send([
          fcl.getTransactionStatus(
            _txHash
          ),
        ])
        .then(fcl.decode);

      if(status.statusString === "SEALED") {
        console.log("Transaction sealed");
        break;
      } else {
        await sleep(500);
      }
    }
  }

  const collectionCreator = async e => {
    e.preventDefault();

    setProcessing(true);

    try {
      const txHash = await fcl.send([
        fcl.transaction`${createCollection}`,
        fcl.args([]),
        fcl.payer(fcl.authz),
        fcl.proposer(fcl.authz),
        fcl.authorizations([ fcl.authz ]),
        fcl.limit(9999)
      ])
      .then(fcl.decode);

      console.log("Hash:: \n", txHash);
      console.log("\nChecking Tx processing...\n");

      await waitingForTx(txHash);
    } catch(e) {
      console.log("ERROR::createCollection::\n", e);
    }

    setProcessing(false);
  }

  const mint = async e => {
    e.preventDefault();

    setProcessing(true);

    try {
      const txHash = await fcl.send([
        fcl.transaction`${minterTx}`,
        fcl.args([ fcl.arg(recipient, types.Address) ]),
        fcl.payer(fcl.authz),
        fcl.proposer(fcl.authz),
        fcl.authorizations([ fcl.authz ]),
        fcl.limit(9999)
      ])
      .then(fcl.decode);

      console.log("Hash:: \n", txHash);
      console.log("\nChecking Tx processing...\n");

      await waitingForTx(txHash);
    } catch(e) {
      console.log("ERROR::Mint::\n");
      console.log(e);
    }

    setProcessing(false);
  }

  return (
    <div>
      <form>
        <div className="form-group">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Recipient's address"
              aria-label="Recipient's address"
              aria-describedby="button-addon2"
              onChange={e => setRecipient(e.target.value)}
            ></input>
            <button
              className="btn btn-primary"
              type="button"
              id="mintSubmit"
              onClick={e => mint(e)}
              disabled={ processing }
            >{ !processing ? "Mint" : "Processing..." }
            </button>
            <button
              className="btn btn-primary"
              type="button"
              id="createCollection"
              onClick={e => collectionCreator(e)}
              disabled={ processing }
            >{ !processing ? "Create Collection" : "Processing..." }</button>
          </div>
        </div>
      </form>
    </div>
  )

}

export default Mint;
