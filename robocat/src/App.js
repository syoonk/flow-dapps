import './App.css';

import Wallet from "./Wallet";
import Mint from "./Mint";
import Assets from "./Assets";

function App({ fcl, types }) {
  return (
    <div className="App">
      <Wallet fcl={ fcl }></Wallet>
      <Mint fcl={ fcl } types={ types }></Mint>
      <Assets fcl={ fcl } types={ types }></Assets>
    </div>
  );
}

export default App;
