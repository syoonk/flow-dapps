import RoboCat from 0x02
import NonFungibleToken from 0x01

transaction(recipient: Address) {

    prepare(acct: AuthAccount) {
        let nftMinter = acct.borrow<&RoboCat.NFTMinter>(from: /storage/RoboCatMinter)
            ?? panic("Cannot borrow NFTMinter resource")

        let collectionRef = getAccount(recipient).getCapability(/public/RoboCatCollection)
            .borrow<&RoboCat.Collection{RoboCat.CollectionPublic}>()
            ?? panic("The account doesn't have RoboCat collection")

        collectionRef.deposit(token: <- nftMinter.createNFT())
    }

    execute {
        log("NFT has been deposited to ".concat(recipient.toString()))
    }
}