import RoboCat from 0x02
import NonFungibleToken from 0x01

transaction {

    prepare(acct: AuthAccount) {
        log("Signer is: ".concat(acct.address.toString()))
        log("Creating Collection...")

        acct.save(<- RoboCat.createEmptyCollection(), to: /storage/RoboCatCollection)
        acct.link<&RoboCat.Collection{RoboCat.CollectionPublic}>(/public/RoboCatCollection, target: /storage/RoboCatCollection)
    }

    execute {
        log("Stored a collection for RoboCat")
    }

}