import RoboCat from 0x02

pub fun main(account: Address): [AnyStruct] {
  let nftCollection = getAccount(account).getCapability(/public/RoboCatCollection)
    .borrow<&RoboCat.Collection{RoboCat.CollectionPublic}>()
    ?? panic("Cannot borrow RoboCat Collection")

  let ids: [UInt64] = nftCollection.getIDs()

  var data: [AnyStruct] = []
  for _id in ids {
    let tokenRef: &RoboCat.NFT = nftCollection.borrowEntireNFT(id: _id)

    data.append({ "name": tokenRef.name, "image": tokenRef.image })
  }

  return data
}
