import NonFungibleToken from 0x01

pub contract RoboCat: NonFungibleToken {

    pub let basicImageUrl: String
    pub let tailImageUrl: String

    pub var totalSupply: UInt64
    pub var nextTokenId: UInt64

    pub event ContractInitialized()
    pub event Withdraw(id: UInt64, from: Address?)
    pub event Deposit(id: UInt64, to: Address?)

    pub resource NFT: NonFungibleToken.INFT {
      pub let id: UInt64
      pub let image: String
      pub let name: String

      init() {
        self.id = RoboCat.nextTokenId
        self.image = RoboCat.basicImageUrl.concat(self.id.toString()).concat(RoboCat.tailImageUrl)
        self.name = "roboCat".concat(self.id.toString())

        RoboCat.nextTokenId = RoboCat.nextTokenId + 1
        RoboCat.totalSupply = RoboCat.totalSupply + 1
      }
    }

    pub resource interface CollectionPublic {
      pub fun deposit(token: @NonFungibleToken.NFT)
      pub fun getIDs(): [UInt64]
      pub fun borrowNFT(id: UInt64): &NonFungibleToken.NFT
      pub fun borrowEntireNFT(id: UInt64): &NFT
    }

    pub resource Collection:
      NonFungibleToken.Provider,
      NonFungibleToken.Receiver,
      NonFungibleToken.CollectionPublic,
      CollectionPublic {
      pub var ownedNFTs: @{UInt64: NonFungibleToken.NFT}

      pub fun withdraw(withdrawID: UInt64): @NonFungibleToken.NFT {
        let token <- self.ownedNFTs.remove(key: withdrawID)
          ?? panic("There is no NFT for given ID")

        emit Withdraw(id: withdrawID, from: token.owner?.address)

        return <- token
      }

      pub fun deposit(token: @NonFungibleToken.NFT) {
        let token <- token as! @RoboCat.NFT

        emit Deposit(id: token.id, to: self.owner?.address)

        self.ownedNFTs[token.id] <-! token
      }

      pub fun getIDs(): [UInt64] {
        return self.ownedNFTs.keys
      }

      pub fun borrowNFT(id: UInt64): &NonFungibleToken.NFT {
        let tokenRef = &self.ownedNFTs[id] as &NonFungibleToken.NFT?
          ?? panic("There is no NFT for given ID");

        return tokenRef
      }

      pub fun borrowEntireNFT(id: UInt64): &NFT {
        let tokenRef = &self.ownedNFTs[id] as auth &NonFungibleToken.NFT?
          ?? panic("There is not NFT for given iD")

        return tokenRef as! &NFT
      }

      init() {
        self.ownedNFTs <- {}
      }

      destroy() {
        destroy self.ownedNFTs
      }
    }

    pub fun createEmptyCollection(): @NonFungibleToken.Collection {
      return <- create RoboCat.Collection()
    }

    pub resource NFTMinter {
      pub fun createNFT(): @NFT {
        return <- create NFT()
      }
    }

    init() {
      self.totalSupply = 0
      self.nextTokenId = 1

      self.basicImageUrl = "https://robohash.org/"
      self.tailImageUrl = "?set=set4"

      emit ContractInitialized()

      self.account.save(<- create NFTMinter(), to: /storage/RoboCatMinter)
    }

}