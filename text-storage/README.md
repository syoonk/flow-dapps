# About
Simple text storage. It contains text and also can be set new text via transaction.

# Config
On `index.js`, modify fcl configuration to fit its proper destination.

# Run
```npm start```

# Contract Deployed Addresses
|TextStorage||
|:---|:---|
|**testnet**|0x7edfdef9a244a201|
