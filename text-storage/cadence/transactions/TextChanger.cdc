import TextStorage from 0xf8d6e0586b0a20c7

transaction(newText: String) {

  prepare(acct: AuthAccount) {}

  execute {
    TextStorage.changeText(newText: newText)
  }

  post {
    TextStorage.someText == newText:
      "Somethings wrong"
  }

}