pub contract TextStorage {

  pub var someText: String

  init() {
    self.someText = "Default Text"
  }

  pub fun changeText(newText: String) {
    self.someText = newText
  }

}