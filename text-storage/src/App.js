import { useState, useEffect } from "react";

function App({ fcl, t }) {

  const [user, setUser] = useState({ addr: "" });
  const [currentText, setCurrentText] = useState("");
  const [newText, setNewText] = useState("");
  const [processing, setProcessing] = useState(false);

  const sleep = ms => {
    return new Promise((r) => setTimeout(r, ms));
  }

  const login = async () => {
    await fcl.authenticate();
  }

  const logout = async () => {
    await fcl.unauthenticate();
  }

  const getText = async () => {
    const rawResponse = await fcl.send([
      fcl.script`
        import TextStorage from 0xTextStorage

        pub fun main(): String {
          let currentText = TextStorage.someText;
          return currentText
        }
        `
      ]);
    const response = await fcl.decode(rawResponse);

    setCurrentText(response);
  }

  const setText = async e => {
    e.preventDefault();

    console.log(newText);

    setProcessing(true);

    let hash;
    try {
      hash = await fcl.mutate({
        cadence: `
          import TextStorage from 0xTextStorage

          transaction(newText: String) {

            prepare(acct: AuthAccount) {}

            execute {
              TextStorage.changeText(newText: newText)
            }

            post {
              TextStorage.someText == newText:
                "Somethings wrong"
            }

          }
        `,
        args: (arg, _t) => [arg(newText, _t.String)],
        proposer: fcl.authz,
        payer: fcl.authz,
        limit: 50
      });
    } catch(err) {
      console.log("Error: ", err);
    }

    console.log("Hash:: \n", hash);
    console.log("\nChecking Tx processing...\n");

    while(true) {
      const status = await fcl
        .send([
          fcl.getTransactionStatus(
            hash
          ),
        ])
        .then(fcl.decode);

      if(status.statusString === "SEALED") {
        console.log("Transaction sealed");
        break;
      } else {
        await sleep(500);
      }
    }

    await getText();

    setProcessing(false);
  }

  const checker = async () => {
    const status = await fcl
      .send([
        fcl.getTransactionStatus(
          "412917fbb36392a5b6534f09471688f4fe43a79aac97c9895df2c7c433217636"
        ),
      ])
      .then(fcl.decode);

      console.log(status);
  }

  useEffect(() => {
    const initPage = async () => {
      fcl.currentUser.subscribe(setUser);

      await getText();
    }
    initPage();
  }, []);

  return (
    <div>
      <button onClick={checker}>hmm</button>
      <div>
      { user.addr && user.addr !== "" ? (
          <h2>Current user is: {user.addr}</h2>
        ) : (
          <h2>Wallet is not connected</h2>
        )
      }
      </div>
      <div>
      { !user.addr || user.addr === "" ? (
          <button
            onClick={login}
            type="button"
            className="btn btn-primary"
          >Login</button>

        ) : (
          <button
            onClick={logout}
            type="button"
            className="btn btn-secondary"
          >Logout</button>
        )
      }
      </div>
      <div>
        <h1>Current Text: {currentText}</h1>
        <button className="btn btn-primary btn-sm" onClick={getText}>Refresh</button>
      </div>
      <form>
        <fieldset>
          <div className="form-group">
            <label className="form-label mt-4">New Text</label>
            <input
              className="form-control"
              placeholder="New Text"
              onChange={e => setNewText(e.target.value)}></input>
            <button
              className="btn btn-primary"
              onClick={e => setText(e)}
              disabled={processing}
            >{processing ? "Processing..." : "Submit"}</button>
          </div>
        </fieldset>
      </form>
    </div>
  );

}

export default App;
