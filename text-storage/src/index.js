import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootswatch/dist/minty/bootstrap.min.css';

import * as fcl from "@onflow/fcl";
import * as t from "@onflow/fcl";

fcl.config({
  "accessNode.api": "https://rest-testnet.onflow.org",
  "discovery.wallet": "https://fcl-discovery.onflow.org/testnet/authn",
  "0xTextStorage": "0x7edfdef9a244a201"
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App fcl={fcl} t={t} />
  </React.StrictMode>
);